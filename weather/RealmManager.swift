//
//  RealmManager.swift
//  weather
//
//  Created by Кирилл on 18.05.21.
//

import Foundation
import RealmSwift

class RealmManager {
    
    static let shared = RealmManager()
    let realm = try! Realm()
    
    private init() { }
    
    func writeObject(weather: WeatherRequests) {
        try! realm.write {
            realm.add(weather)
        }
    }
    
    func readObject() -> [WeatherRequests] {
        return Array(realm.objects(WeatherRequests.self))
    }
}
