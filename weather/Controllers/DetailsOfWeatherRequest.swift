//
//  DetailsOfWeatherRequest.swift
//  weather
//
//  Created by Кирилл on 18.05.21.
//

import UIKit

class DetailsOfWeatherRequest: UIViewController {

    @IBOutlet weak var timeLabel: UILabel!
    
    var currentWeatherRequest: WeatherRequests?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let time = currentWeatherRequest?.time else { return }
        timeLabel.text = "Запрос был сделан: \(time)"
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
