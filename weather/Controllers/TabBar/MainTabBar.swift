//
//  MainTabBar.swift
//  weather
//
//  Created by Кирилл on 20.05.21.
//

import UIKit

class MainTabBar: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        var controllers = [UIViewController]()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        guard let currentLocatinoWeather = storyboard.instantiateViewController(identifier: "CurrentLocationWeather") as? CurrentLocationWeather else { return }
        currentLocatinoWeather.tabBarItem = UITabBarItem(title: "MyLocation", image: UIImage(systemName: "mappin.and.ellipse"), tag: 0)
        let currentLocatinoWeatherNav = UINavigationController(rootViewController: currentLocatinoWeather)
        controllers.append(currentLocatinoWeatherNav)
        
        guard let weatherMap = storyboard.instantiateViewController(identifier: "WeatherMap") as? WeatherMap else { return }
        weatherMap.tabBarItem = UITabBarItem(title: "Map", image: UIImage(systemName: "map"), tag: 1)
        let weatherMapNav = UINavigationController(rootViewController: weatherMap)
        controllers.append(weatherMapNav)

        guard let requestTable = storyboard.instantiateViewController(identifier: "WeatherTable") as? RequestTable else { return }
        requestTable.tabBarItem = UITabBarItem(title: "History", image: UIImage(systemName: "rectangle.grid.1x2"), tag: 2)
        let requestTableNav = UINavigationController(rootViewController: requestTable)
        controllers.append(requestTableNav)
        
        self.viewControllers = controllers
        
    }

}
