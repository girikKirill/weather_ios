//
//  RequestTable.swift
//  weather
//
//  Created by Кирилл on 14.05.21.
//

import UIKit

class RequestTable: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var data: [WeatherRequests] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        
        let nib = UINib(nibName: String(describing: WeatherCell.self), bundle: .main)
        tableView.register(nib, forCellReuseIdentifier: String(describing: WeatherCell.self))
        
        tableView.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        navigationController?.navigationBar.isHidden = false
        
        data = RealmManager.shared.readObject()
        tableView.reloadData()

    }
}

extension RequestTable: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let storyboard = UIStoryboard(name: "Main", bundle: .main)
        guard let vc = storyboard.instantiateViewController(identifier: "RequestDetails") as? DetailsOfWeatherRequest else { return }
        vc.currentWeatherRequest = data[indexPath.row]
        navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension RequestTable: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: WeatherCell.self), for: indexPath)
        guard let weatherCell = cell as? WeatherCell else { return cell }
        weatherCell.setupCell(with: data[indexPath.row])
        return weatherCell
    }
    
    
}
