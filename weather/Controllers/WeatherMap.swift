//
//  ViewController.swift
//  weather
//
//  Created by Кирилл on 13.05.21.
//

import UIKit
import GoogleMaps

class WeatherMap: UIViewController {

    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var requestHistoryButton: UIButton!
    
    var data: [WeatherRequests] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.delegate = self
        
        requestHistoryButton.layer.backgroundColor = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
        requestHistoryButton.layer.cornerRadius = requestHistoryButton.frame.size.height / 2
        requestHistoryButton.alpha = 0.5
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        navigationController?.navigationBar.isHidden = true

    }
    


    @IBAction func openHistoryAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: .main)
        let vc = storyboard.instantiateViewController(identifier: "WeatherTable")
        navigationController?.pushViewController(vc, animated: true)
    }
}

extension WeatherMap: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        
        NetworkManager.shared.getWeather(lat: coordinate.latitude, lon: coordinate.longitude) {currentWeather in
            //вызывается в случае успеха
            guard let temp = currentWeather.main?.temp else { return }
            guard let  place = currentWeather.system?.country else { return }
            guard let icon = currentWeather.weather?[0].icon else  { return }
            let date = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "d MMM, yyyy  HH:mm, VV"
            formatter.timeZone = TimeZone(abbreviation: "UTC+3")
            let myDate = formatter.string(from: date)
            
            let weatherReqest = WeatherRequests(city: currentWeather.name, temp: temp, image: icon, time: myDate)
            RealmManager.shared.writeObject(weather: weatherReqest)
            
            let alert = UIAlertController(title: "\(currentWeather.name), \(place)", message: "Температура: \(temp)", preferredStyle: .alert)
            let action = UIAlertAction(title: "ok", style: .cancel, handler: nil)
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
            
            print("успех")
        } failure: {
            //вызывается в случае
            print("неуспех")

        }

    }
}

