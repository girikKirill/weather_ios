//
//  NetworkManager.swift
//  weather
//
//  Created by Кирилл on 13.05.21.
//

import Foundation
import Moya
import Moya_ObjectMapper

class NetworkManager {
    static let shared  = NetworkManager()
    private let provider = MoyaProvider<WeatherAPI>(plugins: [NetworkLoggerPlugin()])
    private init() { }
    
    func getWeather(lat: Double, lon: Double, completion: @escaping (WeatherData) -> Void, failure: @escaping () -> Void) {
        provider.request(.getWeather(lat: lat, lon: lon)) { (result) in
            switch result {
            case let .success(response):
                guard let weather = try? response.mapObject(WeatherData.self) else {
                    failure()
                    return
                }
                completion(weather)
            case .failure(let error):
                failure()
            }
        }
    }
}
