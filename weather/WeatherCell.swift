//
//  WeatherCell.swift
//  weather
//
//  Created by Кирилл on 18.05.21.
//

import UIKit
import SDWebImage

class WeatherCell: UITableViewCell {

    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var weatherImage: UIImageView!
    @IBOutlet weak var cellBackGroundView: UIView!
    
    
    var currentWeather: WeatherRequests?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.accessoryType = .disclosureIndicator
  
        cellBackGroundView.layer.cornerRadius = 20
        cellBackGroundView.alpha = 0.7
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupCell(with: WeatherRequests) {
        let url = "https://openweathermap.org/img/wn/\(String(with.image))@2x.png"
        cityLabel.text = "\(with.city)"
        tempLabel.text = "\(with.temp) °С"
        weatherImage.sd_setImage(with: URL(string: url))
    }
    
}
