//
//  Weather.swift
//  weather
//
//  Created by Кирилл on 14.05.21.
//

import Foundation
import RealmSwift

class WeatherRequests: Object {
    
    @objc dynamic var city: String = ""
    @objc dynamic var temp: Double = 0.0
    @objc dynamic var image: String = ""
    @objc dynamic var time: String = ""
    

    
    convenience init(city: String, temp: Double, image: String, time: String) {
        self.init()
        self.city = city
        self.temp = temp
        self.image = image
        self.time = time

    }
}
